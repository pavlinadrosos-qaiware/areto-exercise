# Areto Payment Gateway Simple Implementation with Stripe

The solution consists of .NET 5 ASP.NET Core server API, which implements payment create, authorize, capture and cancel endpoints with payment provider Stripe, and a simple React client, which demonstrates how to authorize a created payment with Stripe.js

## .NET 5 ASP.NET Core Server API

The server application uses Entity Framework Core with migrations. Before starting the API server, the database must be updated via executing `dotnet ef database update` in the `Areto.PaymentGateway.Data` project directory.

Payment authorization happens via a server webhook API endpoint, when a payment is created via the server API and the user enters card data and performs 3DS authentication via the frontend and the Stripe.js Card element.

Stripe sends webhook events to configured backend URLs. The easiest way to receive Stripe webhook events from a local environment is to install the Stripe CLI, listen to webhook events and forward them to the local server webhook API endpoint URL.

[Install Stripe CLI](https://stripe.com/docs/stripe-cli)

[Listen to Stripe webhook events](https://stripe.com/docs/stripe-cli/webhooks)

[Server API Swagger](https://localhost:5001/swagger/index.htmls)

## React with Stripe.js Client

The React client uses the server API to create a payment and let the user authorize it via the Stripe.js Card Element. 3DS authentication is performed if needed.

After a successful authorization Stripe sends webhook event to the server, the payment status is changed to Authorized and the payment can be captured via the sever API.

The server API URL is configured in `package.json`.
