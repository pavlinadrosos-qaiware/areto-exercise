import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import './App.css';

function App() {
  const stripe = useStripe();
  const elements = useElements();

  const CARD_ELEMENT_OPTIONS = {
    style: {
      base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4',
        },
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a',
      },
    },
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const {
      paymentProviderData: { clientSecret },
    } = await createPayment();
    if (clientSecret) {
      const { error } = await stripe.confirmCardPayment(
        clientSecret,
        {
          payment_method: {
            card: elements.getElement(CardElement),
          },
        }
      );

      if (!error) {
        alert('Payment created and authorized');
      }
    }
  };

  const createPayment = async () => {
    const response = await fetch('/api/payments', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${btoa('user:password')}`,
      },
      body: JSON.stringify({
        paymentProvider: 'Stripe',
        paymentMethod: 'Card',
        currency: 'EUR',
        amount: 1000,
      }),
    });

    const data = await response.json();

    if (!response.ok) {
      console.log(response);
      console.log('createPayment: Server responded with error');

      return null;
    }

    return data;
  };

  return (
    <div className="App">
      <div style={{
        width: 300,
        padding: 50,
        margin: 'auto',
      }}>
        <form onSubmit={handleSubmit}>
          <CardElement options={CARD_ELEMENT_OPTIONS} />
          <button type="submit" disabled={!stripe} style={{
            margin: 20
          }}>
            Create and Authorize Payment <br/>
            for 10 EUR
          </button>
        </form>
      </div>
    </div>
  );
}

export default App;
