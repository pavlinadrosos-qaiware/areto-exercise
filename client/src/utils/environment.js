export default function getEnv(envName) {
    return process.env[envName];
}
