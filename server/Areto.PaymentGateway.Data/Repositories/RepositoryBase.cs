using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Contracts.Data;
using Areto.PaymentGateway.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Areto.PaymentGateway.Data.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : EntityBase
    {
        protected readonly DbContext DbContext;

        protected readonly DbSet<T> DbSet;
        
        protected RepositoryBase(DbContext dbContext)
        {
            this.DbContext = dbContext;
            this.DbSet = dbContext.Set<T>();
        }

        public virtual IEnumerable<T> GetAll()
            => this.DbSet.ToList();
        
        public virtual async Task<IEnumerable<T>> GetAllAsync()
            => await this.DbSet.ToListAsync();

        public virtual T GetById(Guid id) 
            => this.DbSet.Find(id);

        public virtual T GetById(long id)
            => this.DbSet.Find(id);
        
        public virtual async Task<T> GetByIdAsync(Guid id)
            => await this.DbSet.FindAsync(id);

        public virtual async Task<T> GetByIdAsync(long id)
            => await this.DbSet.FindAsync(id);

        public virtual void Add(T entity)
            => this.DbSet.Add(entity);
        
        public virtual async Task AddAsync(T entity)
            => await this.DbSet.AddAsync(entity);

        public virtual void AddRange(IEnumerable<T> entities)
            => this.DbSet.AddRange(entities);
        
        public virtual async Task AddRangeAsync(IEnumerable<T> entities)
            => await this.DbSet.AddRangeAsync(entities);

        public virtual void Update(T entity) 
            => this.DbSet.Update(entity);
        
        public virtual void UpdateRange(IEnumerable<T> entities)
            => this.DbSet.UpdateRange(entities);

        public virtual void Remove(T entity) 
            => this.DbSet.Remove(entity);
        
        public virtual void RemoveRange(IEnumerable<T> entities)
            => this.DbSet.RemoveRange(entities);

        public void SaveChanges() 
            => this.DbContext.SaveChanges();

        public async Task SaveChangesAsync()
            => await this.DbContext.SaveChangesAsync();

        public void ChangeEntityState(T entity, EntityState state)
        {
            var entry = this.DbContext.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                this.DbSet.Attach(entity);
            }

            entry.State = state;
        }
    }
}