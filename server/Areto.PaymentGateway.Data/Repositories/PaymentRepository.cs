using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Entities;
using Areto.PaymentGateway.Core.Contracts.Data;
using Microsoft.EntityFrameworkCore;

namespace Areto.PaymentGateway.Data.Repositories
{
    public class PaymentRepository: RepositoryBase<Payment>, IPaymentRepository
    {
        public PaymentRepository(PaymentGatewayDbContext dbContext) : base(dbContext)
        {
        }
        
        public virtual async Task<Payment?> GetByPaymentProviderIdAsync(string paymentProviderId)
            => await this.DbSet.SingleOrDefaultAsync(p => p.PaymentProviderId == paymentProviderId);
    }
}