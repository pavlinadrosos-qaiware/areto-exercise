using Areto.PaymentGateway.Core.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Areto.PaymentGateway.Data.Configurations
{
    internal class PaymentConfiguration : EntityConfiguration<Payment>
    {
        public override void Configure(EntityTypeBuilder<Payment> builder)
        {
            base.Configure(builder);
            
            builder.HasIndex(e => new { e.PaymentProviderId })
                .IsUnique(true);

            builder
                .Property(e => e.PaymentProvider)
                .HasConversion<string>();
            
            builder
                .Property(e => e.PaymentMethod)
                .HasConversion<string>();
            
            builder
                .Property(e => e.Currency)
                .IsRequired();
            
            builder
                .Property(e => e.Status)
                .HasConversion<string>();
        }
    }
}