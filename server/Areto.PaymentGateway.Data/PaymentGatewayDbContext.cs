using Areto.PaymentGateway.Data.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Areto.PaymentGateway.Data
{
    public class PaymentGatewayDbContext : DbContext
    {
        public PaymentGatewayDbContext(DbContextOptions<PaymentGatewayDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.ApplyConfiguration(new PaymentConfiguration());
        }
    }
}
