using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Areto.PaymentGateway.Data
{
    public class PaymentGatewayDesignTimeDbContextFactory : IDesignTimeDbContextFactory<PaymentGatewayDbContext>
    {
        public PaymentGatewayDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("dbsettings.json", optional: false, reloadOnChange: true)
                .AddCommandLine(args)
                .Build();

            var connectionString = configuration.GetConnectionString("PaymentGatewayConnection");

            var optionsBuilder = new DbContextOptionsBuilder<PaymentGatewayDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            return new PaymentGatewayDbContext(optionsBuilder.Options);
        }
    }
}