﻿using System;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;
using Areto.PaymentGateway.PaymentProviderStripe;
using Microsoft.Extensions.Configuration;

namespace Areto.PaymentGateway.PaymentProviders
{
    public class PaymentProviderServiceFactory : IPaymentProviderServiceFactory
    {
        private readonly StripeOptions stripeOptions;
        
        public PaymentProviderServiceFactory()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("stripesettings.json", optional: false, reloadOnChange: true)
                .Build();
            
            this.stripeOptions = new StripeOptions();
            configuration.GetSection(StripeOptions.Section).Bind(this.stripeOptions);
        }
        
        public IPaymentProviderService Create(PaymentProvider paymentProvider)
        {
            return paymentProvider switch
            {
                PaymentProvider.Stripe => new StripePaymentProviderServiceService(this.stripeOptions),
                _ => throw new ArgumentException($"Unsupported payment provider {paymentProvider}")
            };
        }
    }
}