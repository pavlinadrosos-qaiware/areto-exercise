using System.ComponentModel.DataAnnotations;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;

namespace Areto.PaymentGateway.Api.RequestModels
{
    public record PaymentCreateModel
    {
        [Required]
        [EnumDataType(typeof(PaymentProvider))]
        public PaymentProvider PaymentProvider { get; set; }
        
        [Required]
        [EnumDataType(typeof(PaymentMethod))]
        public PaymentMethod PaymentMethod { get; set; }
        
        [Required]
        [Range(0, long.MaxValue)]
        public long Amount { get; set; }

        [Required] public string Currency { get; set; } = "";
    }
}