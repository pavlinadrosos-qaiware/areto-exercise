using System;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;
using Areto.PaymentGateway.Core.Entities;

namespace Areto.PaymentGateway.Api.ResponseModels
{
    public record PaymentWithProviderDataModel(
        Guid Id,
        PaymentProvider PaymentProvider, 
        PaymentMethod PaymentMethod, 
        long Amount, 
        string Currency,
        PaymentStatus Status,
        string? PaymentProviderId,
        object PaymentProviderData);
}