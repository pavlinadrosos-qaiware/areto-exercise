using System.Net;

namespace Areto.PaymentGateway.Api.ResponseModels
{
    public record Error(HttpStatusCode Code, string Message);
}