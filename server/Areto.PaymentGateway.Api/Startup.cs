using System.Collections.Generic;
using System.Security.Claims;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Contracts.Data;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;
using Areto.PaymentGateway.Core.Contracts.Services;
using Areto.PaymentGateway.Core.Services;
using Areto.PaymentGateway.Data;
using Areto.PaymentGateway.Data.Repositories;
using Areto.PaymentGateway.PaymentProviders;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ZNetCS.AspNetCore.Authentication.Basic;
using ZNetCS.AspNetCore.Authentication.Basic.Events;

namespace Areto.PaymentGateway.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .AddJsonOptions(o =>
                {
                    o.JsonSerializerOptions.IgnoreNullValues = true;
                    o.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Areto Payment Gateway Areto.PaymentGateway.Api", Version = "v1" });
            });
            
            // Basic Authentication
            services
                .AddAuthentication(BasicAuthenticationDefaults.AuthenticationScheme)
                .AddBasicAuthentication(
                    options =>
                    {
                        options.Realm = "Areto Payment Gateway";
                        options.Events = new BasicAuthenticationEvents
                        {
                            OnValidatePrincipal = context =>
                            {
                                if ((context.UserName == "user") && (context.Password == "password"))
                                {
                                    var claims = new List<Claim>
                                    {
                                        new(ClaimTypes.Name, context.UserName, context.Options.ClaimsIssuer)
                                    };

                                    var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, context.Scheme.Name));
                                    context.Principal = principal;
                                }
                                else 
                                {
                                    // optional with following default.
                                    context.AuthenticationFailMessage = "Authentication failed."; 
                                }

                                return Task.CompletedTask;
                            }
                        };
                    });

            // DB Context
            services.AddDatabaseDeveloperPageExceptionFilter();
            
            string connectionString = Configuration.GetConnectionString("PaymentGatewayConnection");
            services.AddDbContextPool<PaymentGatewayDbContext>(options =>
                options.UseSqlServer(connectionString));

            // Repositories
            services.AddScoped<IPaymentRepository, PaymentRepository>();

            // Application services
            services.AddTransient<IPaymentProviderServiceFactory, PaymentProviderServiceFactory>();
            services.AddTransient<IPaymentService, PaymentService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Areto Payment Gateway v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}