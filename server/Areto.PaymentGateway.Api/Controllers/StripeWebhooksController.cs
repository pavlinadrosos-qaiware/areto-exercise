using System.IO;
using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Contracts.Services;
using Microsoft.AspNetCore.Mvc;
using Stripe;

namespace Areto.PaymentGateway.Api.Controllers
{
    [ApiController]
    [Route("webhooks/stripe")]
    public class StripeWebhooksController : Controller
    {
        private readonly IPaymentService paymentService;

        public StripeWebhooksController(IPaymentService paymentService)
        {
            this.paymentService = paymentService;
        }

        [HttpPost]
        public async Task<ActionResult> HandlePaymentIntent()
        {
            var json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();
            var stripeEvent = EventUtility.ParseEvent(json);
            
            if (stripeEvent.Type != Events.PaymentIntentAmountCapturableUpdated)
            {
                return Ok();
            }

            if (stripeEvent.Data.Object is not PaymentIntent paymentIntent)
            {
                return BadRequest();
            }
                
            var payment = await this.paymentService.GetPaymentByPaymentProviderIdAsync(paymentIntent.Id);
            if (payment == null)
            {
                return Ok();
            }
            
            await this.paymentService.AuthorizePayment(payment);

            return Ok();
        }
    }
}