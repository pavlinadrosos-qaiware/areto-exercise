using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Areto.PaymentGateway.Api.RequestModels;
using Areto.PaymentGateway.Api.ResponseModels;
using Areto.PaymentGateway.Core.Contracts.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Areto.PaymentGateway.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/payments")]
    public class PaymentsController: ControllerBase
    {
        private readonly IPaymentService paymentService;

        public PaymentsController(IPaymentService paymentService)
        {
            this.paymentService = paymentService;
        }
        
        [HttpGet]
        public async Task<IEnumerable<PaymentModel>> GetPayments()
        {
            var payments = await this.paymentService.GetPaymentsAsync();
            
            return payments.Select(p => new PaymentModel(
                p.Id,
                p.PaymentProvider,
                p.PaymentMethod,
                p.Amount,
                p.Currency,
                p.Status,
                p.PaymentProviderId));
        }

        [HttpGet]
        [Route("{id}", Name = "getPaymentRoute")]
        public async Task<ActionResult<PaymentModel>> GetPayment(Guid id)
        {
            var payment = await this.paymentService.GetPaymentAsync(id);
            if (payment == null)
            {
                return NotFound();
            }
            
            return new PaymentModel(
                payment.Id,
                payment.PaymentProvider,
                payment.PaymentMethod, 
                payment.Amount, 
                payment.Currency,
                payment.Status,
                payment.PaymentProviderId);
        }

        [HttpPost]
        public async Task<ActionResult<PaymentWithProviderDataModel>> CreatePayment(PaymentCreateModel paymentCreateModel)
        {
            var result = await this.paymentService.CreatePaymentAsync(paymentCreateModel.PaymentProvider,
                paymentCreateModel.PaymentMethod,
                paymentCreateModel.Amount,
                paymentCreateModel.Currency);
            if (result.IsFailure)
            {
                return BadRequest(new Error(HttpStatusCode.BadRequest, result.Error));
            }

            var (payment, paymentProviderData) = result.Value;

            var model = new PaymentWithProviderDataModel(
                payment.Id,
                payment.PaymentProvider,
                payment.PaymentMethod, 
                payment.Amount, 
                payment.Currency,
                payment.Status,
                payment.PaymentProviderId,
                paymentProviderData);
            
            return CreatedAtRoute("getPaymentRoute", new { id = payment.Id }, model); 
        }

        [HttpPost]
        [Route("{id}/capture")]
        public async Task<ActionResult<PaymentModel>> CapturePayment(Guid id)
        {
            var payment = await this.paymentService.GetPaymentAsync(id);
            if (payment == null)
            {
                return NotFound();
            }

            var result = await this.paymentService.CapturePayment(payment);
            if (result.IsFailure)
            {
                return BadRequest(new Error(HttpStatusCode.BadRequest, result.Error));
            }
            
            payment = result.Value;
            
            return new PaymentModel(
                payment.Id,
                payment.PaymentProvider,
                payment.PaymentMethod, 
                payment.Amount, 
                payment.Currency,
                payment.Status,
                payment.PaymentProviderId); 
        }

        [HttpPost]
        [Route("{id}/cancel")]
        public async Task<ActionResult<PaymentModel>> CancelPayment(Guid id)
        {
            var payment = await this.paymentService.GetPaymentAsync(id);
            if (payment == null)
            {
                return NotFound();
            }
            
            var result = await this.paymentService.CancelPayment(payment);
            if (result.IsFailure)
            {
                return BadRequest(new Error(HttpStatusCode.BadRequest, result.Error));
            }

            payment = result.Value;
            
            return new PaymentModel(
                payment.Id,
                payment.PaymentProvider,
                payment.PaymentMethod, 
                payment.Amount, 
                payment.Currency,
                payment.Status,
                payment.PaymentProviderId);
        }
    }
}