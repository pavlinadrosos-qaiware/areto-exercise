﻿using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;
using Areto.PaymentGateway.Core.Entities;
using CSharpFunctionalExtensions;
using Stripe;

namespace Areto.PaymentGateway.PaymentProviderStripe
{
    public class StripePaymentProviderServiceService : IPaymentProviderService
    {
        public StripePaymentProviderServiceService(StripeOptions stripeOptions)
        {
            StripeConfiguration.ApiKey = stripeOptions.SecretKey;
        }

        public async Task<Result<(string, object)>> CreatePayment(Payment payment)
        {
            var paymentIntentCreateOptions = new PaymentIntentCreateOptions()
            {
                Amount = payment.Amount,
                Currency = payment.Currency,
                CaptureMethod = "manual",
            };

            try
            {
                var paymentIntentService = new PaymentIntentService();
                var paymentIntent = await paymentIntentService.CreateAsync(paymentIntentCreateOptions);

                return (paymentIntent.Id, new { paymentIntent.ClientSecret });
            }
            catch (StripeException exception)
            {
                return Result.Failure<(string, object)>(exception.StripeError.Message);
            }
        }

        public async Task<Result> CapturePayment(Payment payment)
        {
            var paymentIntentCaptureOptions = new PaymentIntentCaptureOptions()
            {
                AmountToCapture = payment.Amount,
            };

            try
            {
                var paymentIntentService = new PaymentIntentService();
                await paymentIntentService.CaptureAsync(payment.PaymentProviderId, paymentIntentCaptureOptions);
                
                return Result.Success();
            }
            catch (StripeException exception)
            {
                return Result.Failure<(string, object)>(exception.StripeError.Message);
            }
        }

        public async Task<Result> CancelPayment(Payment payment)
        {
            try
            {
                var paymentIntentService = new PaymentIntentService();
                await paymentIntentService.CancelAsync(payment.PaymentProviderId);
                
                return Result.Success();
            }
            catch (StripeException exception)
            {
                return Result.Failure<(string, object)>(exception.StripeError.Message);
            }
        }
    }
}