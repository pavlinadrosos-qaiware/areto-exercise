namespace Areto.PaymentGateway.PaymentProviderStripe
{
    public class StripeOptions
    {
        public const string Section = "Stripe:Api";

        public string PublishableKey { get; set; } = "";

        public string SecretKey { get; set; } = "";
    }
}