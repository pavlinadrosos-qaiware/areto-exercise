using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Contracts.Data;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;
using Areto.PaymentGateway.Core.Entities;
using Areto.PaymentGateway.Core.Services;
using Moq;
using Xunit;

namespace Areto.PaymentGateway.Tests.UnitTests
{
    public class PaymentServiceTest
    {
        [Fact]
        public async Task AuthorizePaymentShouldSucceedWhenPaymentStatusIsPending()
        {
            // Arrange
            var paymentRepositoryMock = new Mock<IPaymentRepository>();
            var paymentProviderServiceFactoryMock = new Mock<IPaymentProviderServiceFactory>();

            var paymentService = new PaymentService(paymentRepositoryMock.Object, paymentProviderServiceFactoryMock.Object);
            var payment = new Payment(PaymentProvider.Stripe, PaymentMethod.Card, 100, "EUR");

            // Act
            var result = await paymentService.AuthorizePayment(payment);

            // Assert
            Assert.True(result.IsSuccess);
        }
        
        [Fact]
        public async Task AuthorizePaymentShouldFailWhenPaymentStatusIsNotPending()
        {
            // Arrange
            var paymentRepositoryMock = new Mock<IPaymentRepository>();
            var paymentProviderServiceFactoryMock = new Mock<IPaymentProviderServiceFactory>();

            var paymentService = new PaymentService(paymentRepositoryMock.Object, paymentProviderServiceFactoryMock.Object);
            var payment = new Payment(PaymentProvider.Stripe, PaymentMethod.Card, 100, "EUR");
            payment.Authorize();

            // Act
            var result = await paymentService.AuthorizePayment(payment);

            // Assert
            Assert.True(result.IsFailure);
        }
        
        // Similar unit tests can be written to test all service methods
    }
}