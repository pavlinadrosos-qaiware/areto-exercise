using System;
using System.Threading.Tasks;
using Areto.PaymentGateway.Api.Controllers;
using Areto.PaymentGateway.Api.ResponseModels;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;
using Areto.PaymentGateway.Core.Contracts.Services;
using Areto.PaymentGateway.Core.Entities;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Areto.PaymentGateway.Tests.UnitTests
{
    public class PaymentControllerTest
    {
        [Fact]
        public async Task CapturePaymentShouldReturnNotFoundWhenPaymentDoesntExist()
        {
            // Arrange
            var paymentId = Guid.Parse("e88b856c-9b20-48cb-85af-dc4dacdc81ca");
            
            var paymentServiceMock = new Mock<IPaymentService>();
            paymentServiceMock
                .Setup(p => p.GetPaymentAsync(paymentId))
                .ReturnsAsync((Payment)null);

            var paymentController = new PaymentsController(paymentServiceMock.Object);

            // Act
            var result = await paymentController.CapturePayment(paymentId);

            // Assert
            Assert.IsType<NotFoundResult>(result.Result);
        }
        
        [Fact]
        public async Task CapturePaymentShouldReturnBadRequestWhenCaptureFails()
        {
            // Arrange
            var paymentId = Guid.Parse("e88b856c-9b20-48cb-85af-dc4dacdc81ca");
            var payment = new Payment(PaymentProvider.Stripe, PaymentMethod.Card, 100, "EUR");
            
            var paymentServiceMock = new Mock<IPaymentService>();
            paymentServiceMock
                .Setup(p => p.GetPaymentAsync(paymentId))
                .ReturnsAsync(payment);
            paymentServiceMock
                .Setup(p => p.CapturePayment(payment))
                .ReturnsAsync(Result.Failure<Payment>("Capture Payment Failed"));

            var paymentController = new PaymentsController(paymentServiceMock.Object);

            // Act
            var result = await paymentController.CapturePayment(paymentId);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async Task CapturePaymentShouldReturnPaymentModelWhenCaptureSucceeds()
        {
            // Arrange
            var paymentId = Guid.Parse("e88b856c-9b20-48cb-85af-dc4dacdc81ca");
            var payment = new Payment(PaymentProvider.Stripe, PaymentMethod.Card, 100, "EUR");
            
            var paymentServiceMock = new Mock<IPaymentService>();
            paymentServiceMock
                .Setup(p => p.GetPaymentAsync(paymentId))
                .ReturnsAsync(payment);
            paymentServiceMock
                .Setup(p => p.CapturePayment(payment))
                .ReturnsAsync(payment);

            var paymentController = new PaymentsController(paymentServiceMock.Object);

            // Act
            var result = await paymentController.CapturePayment(paymentId);

            // Assert
            Assert.IsType<PaymentModel>(result.Value);
        }
        
        // Similar unit tests can be written to test all controller methods
    }
}