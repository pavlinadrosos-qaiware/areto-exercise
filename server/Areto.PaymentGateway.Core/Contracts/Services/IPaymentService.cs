using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;
using Areto.PaymentGateway.Core.Entities;
using CSharpFunctionalExtensions;

namespace Areto.PaymentGateway.Core.Contracts.Services
{
    public interface IPaymentService
    {
        public Task<IEnumerable<Payment>> GetPaymentsAsync();

        public Task<Payment?> GetPaymentAsync(Guid id);

        public Task<Payment?> GetPaymentByPaymentProviderIdAsync(string paymentProviderId);

        public Task<Result<(Payment, object)>> CreatePaymentAsync(
            PaymentProvider paymentProvider,
            PaymentMethod paymentMethod,
            long amount,
            string currency);

        public Task<Result<Payment>> AuthorizePayment(Payment payment);

        public Task<Result<Payment>> CapturePayment(Payment payment);

        public Task<Result<Payment>> CancelPayment(Payment payment);
    }
}