using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Entities;
using CSharpFunctionalExtensions;

namespace Areto.PaymentGateway.Core.Contracts.PaymentProviders
{
    public interface IPaymentProviderService
    {
        public Task<Result<(string, object)>> CreatePayment(Payment payment);

        public Task<Result> CapturePayment(Payment payment);

        public Task<Result> CancelPayment(Payment payment);
    }
}