﻿namespace Areto.PaymentGateway.Core.Contracts.PaymentProviders
{
    public interface IPaymentProviderServiceFactory
    {
        public IPaymentProviderService Create(PaymentProvider paymentProvider);
    }
}
