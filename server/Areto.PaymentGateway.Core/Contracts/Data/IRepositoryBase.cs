using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Entities;

namespace Areto.PaymentGateway.Core.Contracts.Data
{
    public interface IRepositoryBase<T> where T: EntityBase
    {
        public IEnumerable<T> GetAll();
        
        public Task<IEnumerable<T>> GetAllAsync();

        public T GetById(Guid id);
        
        public Task<T> GetByIdAsync(Guid id);

        public T GetById(long id);
        
        public Task<T> GetByIdAsync(long id);

        public void Add(T entity);

        public Task AddAsync(T entity);

        public void AddRange(IEnumerable<T> entities);

        public Task AddRangeAsync(IEnumerable<T> entities);

        public void Update(T entity);

        public void UpdateRange(IEnumerable<T> entities);

        public void Remove(T entity);

        public void RemoveRange(IEnumerable<T> entities);

        public void SaveChanges();

        public Task SaveChangesAsync();
    }
}