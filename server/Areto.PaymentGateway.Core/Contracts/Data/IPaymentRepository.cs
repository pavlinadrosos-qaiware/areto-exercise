using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Entities;

namespace Areto.PaymentGateway.Core.Contracts.Data
{
    public interface IPaymentRepository: IRepositoryBase<Payment>
    {
        public Task<Payment?> GetByPaymentProviderIdAsync(string paymentProviderId);
    }
}