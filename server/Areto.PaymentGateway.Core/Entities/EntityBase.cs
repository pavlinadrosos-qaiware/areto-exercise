using System;

namespace Areto.PaymentGateway.Core.Entities
{
    public abstract class EntityBase
    {
        public Guid Id { get; protected set; }
    }
}