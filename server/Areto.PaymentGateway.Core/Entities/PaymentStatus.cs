namespace Areto.PaymentGateway.Core.Entities
{
    public enum PaymentStatus
    {
        Pending = 1,
        Authorized = 2,
        Captured = 3,
        Cancelled = 4,
        Failed = 5,
    }
}