using System;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;

namespace Areto.PaymentGateway.Core.Entities
{
    public class Payment: EntityBase
    {
        public Payment(PaymentProvider paymentProvider, PaymentMethod paymentMethod, long amount, string currency)
        {
            this.Id = Guid.NewGuid();
            this.PaymentProvider = paymentProvider;
            this.PaymentMethod = paymentMethod;
            this.Amount = amount;
            this.Currency = currency;
            this.Status = PaymentStatus.Pending;
        }
        
        public PaymentProvider PaymentProvider { get; private set; }
        
        public PaymentMethod PaymentMethod { get; private set; }
        
        public long Amount { get; private set; }
        
        public string Currency { get; private set; }
        
        public PaymentStatus Status { get; private set; }
        
        public string? PaymentProviderId { get; set; }

        public Payment Authorize()
        {
            this.Status = PaymentStatus.Authorized;

            return this;
        }

        public Payment Capture()
        {
            this.Status = PaymentStatus.Captured;

            return this;
        }

        public Payment Cancel()
        {
            this.Status = PaymentStatus.Cancelled;

            return this;
        }
    }
}