using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Areto.PaymentGateway.Core.Contracts.Data;
using Areto.PaymentGateway.Core.Contracts.PaymentProviders;
using Areto.PaymentGateway.Core.Contracts.Services;
using Areto.PaymentGateway.Core.Entities;
using CSharpFunctionalExtensions;

namespace Areto.PaymentGateway.Core.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository paymentRepository;

        private readonly IPaymentProviderServiceFactory paymentProviderServiceFactory;

        public PaymentService(IPaymentRepository paymentRepository, IPaymentProviderServiceFactory paymentProviderServiceFactory)
        {
            this.paymentRepository = paymentRepository;
            this.paymentProviderServiceFactory = paymentProviderServiceFactory;
        }

        public async Task<IEnumerable<Payment>> GetPaymentsAsync()
            => await this.paymentRepository.GetAllAsync();

        public async Task<Payment?> GetPaymentAsync(Guid id)
            => await this.paymentRepository.GetByIdAsync(id);
        
        public async Task<Payment?> GetPaymentByPaymentProviderIdAsync(string paymentProviderId)
            => await this.paymentRepository.GetByPaymentProviderIdAsync(paymentProviderId);

        public async Task<Result<(Payment, object)>> CreatePaymentAsync(
            PaymentProvider paymentProvider, 
            PaymentMethod paymentMethod, 
            long amount, 
            string currency)
        {
            var payment = new Payment(paymentProvider, paymentMethod, amount, currency);

            var paymentProviderService = this.paymentProviderServiceFactory.Create(payment.PaymentProvider);
            var result = await paymentProviderService.CreatePayment(payment);
            if (result.IsFailure)
            {
                return result.ConvertFailure<(Payment, object)>();
            }
            
            var (paymentProviderId, paymentProviderData) = result.Value;
                
            payment.PaymentProviderId = paymentProviderId;
            await this.paymentRepository.AddAsync(payment);
            await this.paymentRepository.SaveChangesAsync();

            return (payment, paymentProviderData);
        }
        
        public async Task<Result<Payment>> AuthorizePayment(Payment payment)
        {
            if (payment.Status != PaymentStatus.Pending)
            {
                return Result.Failure<Payment>(
                    $"This Payment could not be authorized because it has a status of {payment.Status}. Only a Payment with status {PaymentStatus.Pending} may be authorized.");
            }

            payment.Authorize();
            await this.paymentRepository.SaveChangesAsync();

            return payment;
        }

        public async Task<Result<Payment>> CapturePayment(Payment payment)
        {
            if (payment.Status != PaymentStatus.Authorized)
            {
                return Result.Failure<Payment>(
                    $"This Payment could not be captured because it has a status of {payment.Status}. Only a Payment with status {PaymentStatus.Authorized} may be captured.");
            }
            
            var paymentProviderService = this.paymentProviderServiceFactory.Create(payment.PaymentProvider);
            var result = await paymentProviderService.CapturePayment(payment);
            if (result.IsFailure)
            {
                return result.ConvertFailure<Payment>();
            }

            payment.Capture();
            await this.paymentRepository.SaveChangesAsync();

            return payment;
        }

        public async Task<Result<Payment>> CancelPayment(Payment payment)
        {
            if (payment.Status != PaymentStatus.Pending && payment.Status != PaymentStatus.Authorized)
            {
                return Result.Failure<Payment>(
                    $"This Payment could not be cancelled because it has a status of {payment.Status}. Only a Payment with status {PaymentStatus.Pending} or {PaymentStatus.Authorized} may be cancelled.");
            }
            
            var paymentProviderService = this.paymentProviderServiceFactory.Create(payment.PaymentProvider);
            var result = await paymentProviderService.CancelPayment(payment);
            if (result.IsFailure)
            {
                return result.ConvertFailure<Payment>();
            }

            payment.Cancel();
            await this.paymentRepository.SaveChangesAsync();

            return payment;
        }
    }
}